import { PrismaClient } from "@prisma/client";
import { hash } from "bcrypt";

const prisma = new PrismaClient();

async function seed() {
  try {
    await prisma.user.create({
      data: {
        email: "john@example.com",
        password: await hash("12345", 10),
        role: "ADMIN",
      },
    });
    console.log("Seed data created successfully.");
  } catch (error) {
    console.error("Error seeding data:", error);
  } finally {
    await prisma.$disconnect();
  }
}

seed()
  .catch((error) => console.error("Error running seed:", error))
  .finally(() => prisma.$disconnect());
