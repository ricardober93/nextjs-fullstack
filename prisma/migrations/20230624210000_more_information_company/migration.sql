/*
  Warnings:

  - A unique constraint covering the columns `[NIT]` on the table `Company` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `NIT` to the `Company` table without a default value. This is not possible if the table is not empty.
  - Added the required column `phone` to the `Company` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Company" ADD COLUMN     "NIT" TEXT NOT NULL,
ADD COLUMN     "phone" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Company_NIT_key" ON "Company"("NIT");
