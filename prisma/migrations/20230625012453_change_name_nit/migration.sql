/*
  Warnings:

  - You are about to drop the column `NIT` on the `Company` table. All the data in the column will be lost.
  - Added the required column `nit` to the `Company` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "Company_NIT_key";

-- AlterTable
ALTER TABLE "Company" DROP COLUMN "NIT",
ADD COLUMN     "nit" TEXT NOT NULL;
