// These styles apply to every route in the application
import ClientOnly from "@/components/client-only";
import Header from "@/components/header";
import "@/styles/globals.css";
import { AuthContextProvider } from "context/auth-context";
import { Metadata } from "next";
import { Inter } from "next/font/google";
import { Suspense } from "react";
import { Toaster } from "react-hot-toast";

const inter = Inter({
  variable: "--font-inter",
  subsets: ["latin"],
});

const title = "Application Inventary";
const description = "This is a Aplication Inventary created with Next.js and TailwindCSS.";

export const metadata: Metadata = {
  title,
  description,
  twitter: {
    card: "summary_large_image",
    title,
    description,
  },
  metadataBase: new URL("https://nextjs-postgres-auth.vercel.app"),
  themeColor: "#FFF",
};

export default async function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body className={inter.variable}>
        <Toaster />
        <ClientOnly>
          <Header />
          <AuthContextProvider>
            <Suspense fallback="Loading...">{children}</Suspense>
          </AuthContextProvider>
        </ClientOnly>
      </body>
    </html>
  );
}
