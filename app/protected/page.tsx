import SignOut from "@/components/sign-out";
import Link from "next/link";


export default async function Home() {

  

  return (
    <div className="flex h-screen bg-black">
      <div className="w-screen h-screen flex flex-col space-y-5 justify-center items-center">
        <Link href={'/'} className="flex h-10 w-full items-center justify-center rounded-md border text-sm transition-all focus:outline-none border-black bg-black text-white hover:bg-white hover:text-black">
          home
        </Link>
        <SignOut />
      </div>
    </div>
  );
}
