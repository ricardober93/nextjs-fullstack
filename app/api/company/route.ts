import prisma from "@/lib/prisma";
import { getServerSession } from "next-auth/next";
import { NextResponse } from "next/server";

export async function GET() {
  const session = await getServerSession();
  if (session) {
    const email = session.user?.email!;

    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });

    if (user) {
      
      const company = await prisma.company.findUnique({
        where: {
          userId: user.id,
        },
      });
  
      return NextResponse.json(company);
    }else{
      return NextResponse.json({ error: "company not found" }, { status: 400 });
    }
  }

  return NextResponse.json("You must be logged in.");
}
