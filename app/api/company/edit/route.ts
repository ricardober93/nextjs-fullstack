import prisma from "@/lib/prisma";
import { NextApiResponse } from "next";
import { getServerSession } from "next-auth/next";
import { NextResponse } from "next/server";

export async function PATCH(req: Request, res: NextApiResponse) {
  const session = await getServerSession();
  const { name, nit, address, phone } = await req.json();
  if (session) {
    const email = session.user?.email!;

    const user = await prisma.user.findUnique({
      where: {
        email,
      },
      include: {
          company: true,
      }
    });


    if (user) {

      const company = await prisma.company.update({
        where: {
        id: user?.company?.id,
        },
        data: {
          name,
          nit,
          address,
          phone,
        },
      });

      return NextResponse.json({ company }, { status: 200 });
      
    }else{
      return NextResponse.json("No exists user");
    }
  }

  return NextResponse.json("You must be logged in.");
}
