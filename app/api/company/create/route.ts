import { NextApiRequest, NextApiResponse } from "next";
import { getServerSession } from "next-auth/next";
import { NextResponse } from "next/server";
import prisma from "@/lib/prisma";

export async function POST(req: Request, res: NextApiResponse) {
  const session = await getServerSession();
  const { name, nit, address, phone } = await req.json();
  if (session) {
    const email = session.user?.email!;

    const user = await prisma.user.findUnique({
      where: {
        email,
      },
    });


    if (user) {

      const company = await prisma.company.create({
        data: {
          name,
          nit,
          address,
          phone,
          user :{
            connect: {
              id  : user.id,
            },
          }
        },
      });

      return NextResponse.json({ company }, { status: 200 });
      
    }else{
      return NextResponse.json("No exists user");
    }
  }

  return NextResponse.json("You must be logged in.");
}
