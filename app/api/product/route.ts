import prisma from "@/lib/prisma";
import { getServerSession } from "next-auth/next";
import { NextResponse } from "next/server";

export async function GET() {
  const session = await getServerSession();
  if (session) {
    const email = session.user?.email!;

    const user = await prisma.user.findUnique({
      where: {
        email,
      },
      include: {
        company: true,
      },
    });
    if (user?.company?.id) {
      const products = await prisma.product.findMany({
        select: {
          id: true,
          name: true,
          quantity: true,
          price: true,
          description: true,
          urlPhoto: true,
        },
        where: {
            companyId: user?.company?.id,
        },
        orderBy: {
          name: 'desc',
        },
      });
      return NextResponse.json({ products }, { status: 200 });
    } else {
      return NextResponse.json("No exists product");
    }
  }

  return NextResponse.json("You must be logged in.");
}
