import { NextApiResponse } from "next";
import prisma from "@/lib/prisma";
import { getServerSession } from "next-auth/next";
import { NextResponse } from "next/server";

export async function POST(req: Request, res: NextApiResponse) {
  const session = await getServerSession();
  const { name, quantity, price, description, urlPhoto } = await req.json();
  if (session) {
    const email = session.user?.email!;

    const user = await prisma.user.findUnique({
      where: {
        email,
      },
      include: {
        company: true,
      },
    });
    if (user?.company?.id) {
      const product = await prisma.product.create({
        data: {
          name,
          quantity,
          price,
          description,
          urlPhoto,
          company: {
            connect: {
              id: user?.company?.id,
            },
          },
        },
      });
      console.log(product);
      
      return NextResponse.json({ product }, { status: 200 });
    } else {
      return NextResponse.json("No exists product");
    }
  }

  return NextResponse.json("You must be logged in.");
}
