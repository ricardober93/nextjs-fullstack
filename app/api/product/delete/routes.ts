import prisma from "@/lib/prisma";
import { NextApiResponse } from "next";
import { NextResponse } from "next/server";

export async function DELETE(req: Request, res: NextApiResponse) {
  const { id } = await req.json();

  if (id) {
    const product = await prisma.product.delete({
      where: {
        id: id,
      },
    });

    return NextResponse.json({ product }, { status: 200 });
  } else {
    return NextResponse.json("No exists product");
  }
}
