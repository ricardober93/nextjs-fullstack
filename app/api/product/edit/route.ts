import prisma from "@/lib/prisma";
import { NextApiResponse } from "next";
import { getServerSession } from "next-auth/next";
import { NextResponse } from "next/server";

export async function PATCH(req: Request, res: NextApiResponse) {
  const { id, name, quantity, price, description } = await req.json();

  if (id) {
    const product = await prisma.product.update({
      where: {
        id: id,
      },
      data: {
        name,
        quantity,
        price,
        description,
      },
    });

    return NextResponse.json({ product }, { status: 200 });
  } else {
    return NextResponse.json("No exists user");
  }
}
