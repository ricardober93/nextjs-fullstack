
import ListCompany from "@/components/home/list-company";
import ListInventary from "@/components/home/list-inventary";
import { Suspense } from "react";

export default async function Home() {
  return (
    <div className="flex h-screen">
      <div className="w-screen h-screen flex flex-col">
        <ListCompany />
        <section className="flex flex-1 w-full">
          <ListInventary />
        </section>
      </div>
    </div>
  );
}
