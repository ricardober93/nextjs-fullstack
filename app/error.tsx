"use client"; // Error components must be Client Components


export default function Error({ error, reset }: { error: Error; reset: () => void }) {

  return (
    <div className="flex h-screen">
      <div className="w-screen h-screen flex flex-col justify-center items-center">
        <h2>Something went wrong!</h2>
        <p>
        </p>
        <button
          onClick={
            // Attempt to recover by trying to re-render the segment
            () => reset()
          }>
          Try again
        </button>
      </div>
    </div>
  );
}
