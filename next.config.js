/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    appDir: true,
  },
  ignoreBuildErrors: true,
  env: {
    DATABASE_URL: process.env.DATABASE_URL,
    DIRECT_URL: process.env.DIRECT_URL,
    SHADOW_DATABASE_URL: process.env.SHADOW_DATABASE_URL,
    SUPERBASE_URL: process.env.SUPERBASE_URL,
    SUPERBASE_PUBLIC_KEY: process.env.SUPERBASE_PUBLIC_KEY,
  },
  swcMinify: true,
};

module.exports = nextConfig;
