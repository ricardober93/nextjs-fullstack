'use client';
import { User } from "model/user-model";
import { createContext, useContext, useState } from "react";

export type authContextType = {
    user: User | null;
     setUser: (user: User | null) => void;
};

export const AuthContext = createContext<authContextType>({
    user: null,
    setUser: () => {},
})

export const useAuthContext = () => useContext(AuthContext);

export const AuthContextProvider = ({
    children,
}: { children: React.ReactNode }) => {
    const [user, setUser] = useState<User | null>(null);


    return (
        <AuthContext.Provider value={{ user, setUser }}> 
        {children}
        </AuthContext.Provider>
    );
   
};
