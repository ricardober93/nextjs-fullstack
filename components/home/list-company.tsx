"use client";
import { useEffect, useState } from "react";
import CreateCompany from "../modal/comapy-add";

type Company = {
  id: number;
  name: string;
  address: string;
  nit: string;
  phone: string;
  userId: number | null;
}
export default function ListCompany() {
  const [company, setCompany] = useState<Company | null>(null);

  const getCompany = async () => {
    try {
      const res = await fetch("/api/company", {});
      const companyData = await res.json();
      companyData && setCompany(companyData);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getCompany();
  }, []);

  if (company === null) {
    return (
      <div className="w-full flex justify-center items-center">
        <CreateCompany type="new" />
      </div>
    );
  }

  return (
    <div className="w-full flex flex-col p-4">
      <div className="w-full py-4 px-8">
        <div className="px-4 sm:px-0 flex justify-between items-center">
          <h3 className="text-base font-semibold leading-7 text-gray-200">Company Information</h3>
          <CreateCompany
            type="edit"
            getCompany={getCompany}
            defaultValues={company}
          />
        </div>
        <div className="mt-6 border-t border-gray-100">
          <div className="divide-y divide-gray-100">
            <div className="px-4 py-6 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <p className="text-sm font-medium leading-6 text-gray-200">Full name</p>
              <p className="mt-1 text-sm leading-6 text-gray-100 sm:col-span-2 sm:mt-0">{company.name}</p>
            </div>
            <div className="px-4 py-6 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <p className="text-sm font-medium leading-6 text-gray-200">NIT:</p>
              <p className="mt-1 text-sm leading-6 text-gray-100 sm:col-span-2 sm:mt-0">{company.nit}</p>
            </div>
            <div className="px-4 py-6 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <p className="text-sm font-medium leading-6 text-gray-200">Address</p>
              <p className="mt-1 text-sm leading-6 text-gray-100 sm:col-span-2 sm:mt-0">{company.address}</p>
            </div>
            <div className="px-4 py-6 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
              <p className="text-sm font-medium leading-6 text-gray-200">Phone</p>
              <p className="mt-1 text-sm leading-6 text-gray-100 sm:col-span-2 sm:mt-0">{company.phone}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
