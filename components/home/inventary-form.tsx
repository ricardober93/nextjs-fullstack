"use client";

import { superbase } from "@/lib/superBase";
import { useRouter } from "next/navigation";
import { FormEvent, useState } from "react";
import { toast } from "react-hot-toast";
import LoadingDots from "../loading-dots";

export default function InventaryForm({getProducts }: {getProducts: ()=> void}) {
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const createProduct = async (e : any) => {
    e.preventDefault();

    const { name, quantity, price, description, urlPhoto } = e.currentTarget;
    let url;
    if (urlPhoto && urlPhoto.files![0]) {
      const name = urlPhoto?.files![0].name;
      const image = urlPhoto?.files![0];
      const { data, error } = await superbase.storage.from("images").upload(`public/${name}`, image, {
        cacheControl: "3600",
        upsert: false,
      });
      url = data?.path;
    }

    fetch("/api/product/create", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name.value,
        quantity: quantity.value, 
        price: price.value,
        description: description.value,
        urlPhoto: url ? url : "",
      })
    }).then( (res)=> {
      toast.success("Producto created! Redirecting to login...");
      getProducts()
    }).catch(
      (err) => {
        console.log(err);
        
      }
    );
  };
  return (
    <section className="w-4/5 mx-auto p-4">
      <h1 className="text-xl font-bold mb-4">Crear un producto</h1>
      <form
        onSubmit={(e) => createProduct(e)}
        className="w-full flex flex-col gap-3">
        <div className="w-full flex flex-col md:flex-row gap-3 ">
          <div className="w-full md:w-[33%]">
            <label
              htmlFor="name"
              className="block text-xs text-gray-600 uppercase">
              Nombre del producto
            </label>
            <input
              id="name"
              name="name"
              type="text"
              placeholder="Libros, carro, Camisa ..."
              required
              className="input input-bordered input-primary w-full"
            />
          </div>

          <div className="w-full md:w-[33%]">
            <label
              htmlFor="quantity"
              className="block text-xs text-gray-600 uppercase">
              Cantidad
            </label>
            <input
              id="quantity"
              name="quantity"
              type="number"
              placeholder="34563"
              className="input input-bordered input-primary w-full"
            />
          </div>

          <div className="w-full md:w-[33%]">
            <label
              htmlFor="price"
              className="block text-xs text-gray-600 uppercase">
              Precio
            </label>
            <input
              id="price"
              name="price"
              type="number"
              placeholder="34563"
              className="input input-bordered input-primary w-full"
            />
          </div>
        </div>

        <div className="w-full flex  flex-col md:flex-row  gap-3 ">
          <div className="w-full md:w-[50%]">
            <label
              htmlFor="description"
              className="block text-xs text-gray-600 uppercase">
              Descripción del producto
            </label>
            <input
              id="description"
              name="description"
              type="text"
              placeholder="Artículo con calidad..."
              className="input input-bordered input-primary w-full"
            />
          </div>

          <div className="w-full md:w-[50%]">
            <label
              htmlFor="urlPhoto"
              className="block text-xs text-gray-600 uppercase">
              Imagen
            </label>
            <input
              id="urlPhoto"
              name="urlPhoto"
              type="file"
              className="file-input file-input-bordered file-input-primary w-full"
            />
          </div>
        </div>
        <button
          disabled={loading}
          className={`${loading ? "cursor-not-allowed border-gray-200 bg-gray-100" : ""} btn btn-primary`}>
          {loading ? <LoadingDots color="#808080" /> : <p> Crear producto </p>}
        </button>
      </form>
    </section>
  );
}
