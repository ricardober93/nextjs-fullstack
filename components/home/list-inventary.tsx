"use client";
import { Product } from "@prisma/client";
import { useEffect, useState } from "react";
import InventaryForm from "./inventary-form";
import EditProduct from "../modal/edit-product";

export default function ListInventary() {
  const [listProduct, setListProduct] = useState<Product[]>([]);

  const getProducts = async (): Promise<void> => {
    try {
      const res = await fetch("/api/product", {
        headers: { "Content-Type": "application/json" },
      });
      const data = await res.json();
      data && setListProduct(data?.products);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <main className="flex flex-col gap-3 w-full">
      <InventaryForm getProducts={getProducts} />
      <section className="w-4/5 mx-auto p-4">
        <table className="table">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Cantidad</th>
              <th>Precio</th>
              <th>Descripción</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {listProduct?.length > 0 ? (
              listProduct?.map((product) => {
                return (
                  <tr key={product.id + product.name}>
                    <td>
                      <span className="flex items-center space-x-3">
                        <span className="avatar">
                          <span className="mask mask-squircle w-12 h-12">
                            {/* eslint-disable-next-line @next/next/no-img-element */}
                            <img
                              src={"https://bvyzrclekocjczhojzys.supabase.co/storage/v1/object/public/images/" + product.urlPhoto}
                              alt={product.urlPhoto}
                            />
                          </span>
                        </span>
                        <span>
                          <p className="font-bold">{product.name}</p>
                        </span>
                      </span>
                    </td>
                    <td>{product.quantity}</td>
                    <td> {product.price}</td>
                    <td> {product.description}</td>
                    <th>
                      <EditProduct defaultValues={product} getProducts={getProducts}/>
                    </th>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td>
                  <p>No hay productos para mostrar</p>
                </td>
              </tr>
            )}
          </tbody>
        </table>
      </section>
    </main>
  );
}
