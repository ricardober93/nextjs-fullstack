import { Session } from "next-auth";

export default  function AuthStatus({session}:{ session: Session}) {

  
  return (
    <>
        <p className="text-stone-200 text-sm">
          Inicio de sesion como {session?.user?.email}
        </p>
    </>
  );
}
