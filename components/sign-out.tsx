"use client";
import { signOut } from "next-auth/react";

export default function SignOut() {
  return (
    <button
    className="btn"
      onClick={() => signOut()}
    >
     Salir
    </button>
  );
}
