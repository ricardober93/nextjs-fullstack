"use client";

import { Dialog, Transition } from "@headlessui/react";
import { FormEvent, FormEventHandler, Fragment, useState } from "react";
import LoadingDots from "../loading-dots";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";
import { Company } from "@prisma/client";
import { useForm, SubmitHandler } from "react-hook-form";

type Inputs = {
  name: string;
  nit: string;
  address: string;
  phone: string;
};


type Types ={
  type: string,
  defaultValues?: Inputs
  getCompany?: () => void
}

/**
 * Renders a modal with information about a company retrieved from "/api/company".
 *
 * @return {JSX.Element} A div containing the text "CompanyModal".
 */
export default function CreateCompany({type = 'new', defaultValues, getCompany }: Types) {
  const [loading, setLoading] = useState(false);
  let [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  const createCompany = (data: Inputs) => {
    fetch("/api/company/create", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: data.name,
        nit: data.nit,
        address: data.address,
        phone: data.phone,
      }),
    }).then(async (res) => {
      setLoading(false);
      if (res.status === 200) {
        toast.success("Compania creada!");
        setTimeout(() => {
          closeModal();
          router.refresh();
         
        }, 2000);
      } else {
        const { error } = await res.json();
        toast.error(error);
      }
    });
  }

  const editCompany = (data: Inputs) => {
    fetch("/api/company/edit", {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: data.name,
        nit: data.nit,
        address: data.address,
        phone: data.phone,
      }),
    }).then(async (res) => {
      setLoading(false);
      if (res.status === 200) {
        toast.success("Compania actualizada!");
        setTimeout(() => {
          closeModal();
          router.refresh();
          if (getCompany) {
            getCompany();
          }
        }, 2000);
      } else {
        const { error } = await res.json();
        toast.error(error);
      }
    });
  }

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({ defaultValues });
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    console.log(data);
    setLoading(true);

    if (type === 'new') {
      createCompany(data);
    }else{
      editCompany(data);
    }
   
  };

  return (
    <>
      {type === "new" ? (
        <button
          type="button"
          onClick={openModal}
          className="btn btn-primary">
          Crear una compañia
        </button>
      ) : (
        <button
          onClick={openModal}
          type="button"
          className="btn btn-secondary">
          editar una compañia
        </button>
      )}
      <Transition
        appear
        show={isOpen}
        as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0">
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95">
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-neutral border-1 border-solid border-gray-100 p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="mb-5 text-lg font-medium leading-6 text-gray-100">
                    Crear compañia
                  </Dialog.Title>
                  <form  onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-2 grid gap-3 mx-auto">
                      <div>
                        <label
                          htmlFor="email"
                          className="block text-xs text-gray-200 uppercase">
                          Nombre de la compañia
                        </label>
                        <input
                          {...register("name")}
                          defaultValue={defaultValues?.name}
                          name="name"
                          type="text"
                          placeholder="Compañia Sol y playa"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>

                      <div>
                        <label
                          htmlFor="nit"
                          className="block text-xs text-gray-200 uppercase">
                          NIT
                        </label>
                        <input
                          {...register("nit")}
                          name="nit"
                          defaultValue={defaultValues?.nit}
                          type="number"
                          placeholder="23456453432-0"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>

                      <div>
                        <label
                          htmlFor="address"
                          className="block text-xs text-gray-200 uppercase">
                          Dirección
                        </label>
                        <input
                          {...register("address")}
                          defaultValue={defaultValues?.address}
                          name="address"
                          type="text"
                          placeholder="Avenida Siempre viva, Bogota"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>

                      <div>
                        <label
                          htmlFor="phone"
                          className="block text-xs text-gray-200 uppercase">
                          Telefono
                        </label>
                        <input
                          {...register("phone" )}
                          defaultValue={defaultValues?.phone}
                          name="phone"
                          type="text"
                          placeholder="345 345 333"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>
                    </div>

                    <div className="mt-4">
                      <button
                        disabled={loading}
                        className={`${loading ? "cursor-not-allowed border-gray-200 bg-gray-100" : ""} btn btn-primary`}>
                        {loading ? <LoadingDots color="#808080" /> : <p> {type === "new" ? 'Crear compania ' : 'ditar compania '} </p>}
                      </button>
                    </div>
                  </form>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
