"use client";

import { Dialog, Transition } from "@headlessui/react";
import { useRouter } from "next/navigation";
import { Fragment, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import toast from "react-hot-toast";
import LoadingDots from "../loading-dots";

type Inputs = {
  id: number;
  name: string;
  quantity: string;
  price: string;
  description: string;
  urlPhoto: string;
};

type Types = {
  defaultValues?: Inputs;
  getProducts: () => void;
};

/**
 * Renders a modal with information about a company retrieved from "/api/company".
 *
 * @return {JSX.Element} A div containing the text "CompanyModal".
 */
export default function EditProduct({ defaultValues, getProducts }: Types) {
  const [loading, setLoading] = useState(false);
  let [isOpen, setIsOpen] = useState(false);
  const router = useRouter();
  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  const editProduct = (data: Inputs) => {
    fetch("/api/product/edit", {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        id: defaultValues?.id,
        name: data.name,
        quantity: data.quantity,
        price: data.price,
        description: data.description,
      }),
    }).then(async (res) => {
      setLoading(false);
      if (res.status === 200) {
        toast.success("prodcuto Editado!");
        setTimeout(() => {
          closeModal();
          router.refresh();
          getProducts();
        }, 2000);
      } else {
        const { error } = await res.json();
        toast.error(error);
      }
    });
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Inputs>({ defaultValues });
  const onSubmit: SubmitHandler<Inputs> = (data) => {
    console.log(data);
    setLoading(true);
    editProduct(data);
  };

  return (
    <>
      <button
        onClick={openModal}
        type="button"
        className="btn btn-ghost btn-xs">
        editar
      </button>

      <Transition
        appear
        show={isOpen}
        as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0">
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto">
            <div className="flex min-h-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95">
                <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-2xl bg-neutral border-1 border-solid border-gray-100 p-6 text-left align-middle shadow-xl transition-all">
                  <Dialog.Title
                    as="h3"
                    className="mb-5 text-lg font-medium leading-6 text-gray-100">
                    Editar producto
                  </Dialog.Title>
                  <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mt-2 grid gap-3 mx-auto">
                      <div>
                        <label
                          htmlFor="email"
                          className="block text-xs text-gray-200 uppercase">
                          Nombre
                        </label>
                        <input
                          {...register("name")}
                          defaultValue={defaultValues?.name}
                          name="name"
                          type="text"
                          placeholder="Compañia Sol y playa"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>

                      <div>
                        <label
                          htmlFor="quantity"
                          className="block text-xs text-gray-200 uppercase">
                          Cantidad
                        </label>
                        <input
                          {...register("quantity")}
                          name="quantity"
                          defaultValue={defaultValues?.quantity}
                          type="number"
                          placeholder="23456453432-0"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>

                      <div>
                        <label
                          htmlFor="price"
                          className="block text-xs text-gray-200 uppercase">
                          Precio
                        </label>
                        <input
                          {...register("price")}
                          defaultValue={defaultValues?.price}
                          name="price"
                          type="number"
                          placeholder="Avenida Siempre viva, Bogota"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>

                      <div>
                        <label
                          htmlFor="description"
                          className="block text-xs text-gray-200 uppercase">
                          descripción
                        </label>
                        <input
                          {...register("description")}
                          defaultValue={defaultValues?.description}
                          name="description"
                          type="text"
                          placeholder="345 345 333"
                          required
                          className="input input-bordered w-full max-w-xs"
                        />
                      </div>
                    </div>

                    <div className="mt-4">
                      <button
                        disabled={loading}
                        className={`${loading ? "cursor-not-allowed border-gray-200 bg-gray-100" : ""} btn btn-primary`}>
                        {loading ? <LoadingDots color="#808080" /> : <p> Eitar producto </p>}
                      </button>
                    </div>
                  </form>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
}
