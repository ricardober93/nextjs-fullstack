
import { authOptions } from "@/app/api/auth/[...nextauth]/route";
import { getServerSession } from "next-auth";
import AuthStatus from "./auth-status";
import SignOut from "./sign-out";

export default async function Header() {
  const session = await getServerSession(authOptions);
  
  return (
    <>
      {session ? (
    <div className="navbar bg-base-100">
    <div className="navbar-start">
      <div className="dropdown">
        <label tabIndex={0} className="btn btn-ghost lg:hidden">
          <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h8m-8 6h16" /></svg>
        </label>
      </div>
      <a className="btn btn-ghost normal-case text-xl">daisyUI</a>
        <AuthStatus session={session} />
    </div>
    <div className="navbar-end">
    <SignOut />
    </div>
  </div> ) : null
  }
  </>
  )
}
