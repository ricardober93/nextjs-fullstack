import { createClient } from "@supabase/supabase-js";


export const superbase = createClient(
  process.env.SUPERBASE_URL,
  process.env.SUPERBASE_PUBLIC_KEY);
